# MasterClass

An application for Android TV. It is a catalogue of self-development youtube videos with easy search, recommendations and youtube player. 
It is educational application which became a fianlist of the II GS Group all-Russian programming competition 2016 ([link](http://programming.gs-group.com/)). 
It can be used as template for your Android TV catalogue application.

![Alt text](app/src/main/res/drawable/banner.png?raw=true "Application banner")

[A short demo video for this application](https://photos.google.com/photo/AF1QipPxjlgGg-egeHo1Ptwk6QMMYilQ5RSTQCaivU-R).

# Requirements:

Android 4 (API level 17) or higher. 

Program was developed specially for devices controlled by remote (DPAD). It is workable on sensor devices, but there may be problems with navigation (not smooth lists scrolling, buttons may require double click for activation).

Devices with less than 8'' diagonal are not supported. On such devices some elements may be displayed wrong.

This program uses Youtube API, so the Youtube application should be installed on a device.
[Here is the link](https://play.google.com/store/apps/details?id=com.google.android.youtube&hl=ru) to general Youtube app in Google Play;
[here is the link](https://play.google.com/store/apps/details?id=com.google.android.youtube.googletv&hl=ru) to Google TV version of Youtube; but note, that [application «YouTube for Android TV»](https://play.google.com/store/apps/details?id=com.google.android.youtube.tv&hl=ru) does not support Youtube API. So if this version is installed you should replace it with one of above ones. Some smart TVs do not have needed version of Youtube in Google Play and in this case you should install supported version of Youtube from external sources.

Before application compilation you should register as Youtube developer and get Youtube developer id (for further info visit [this website](https://developers.google.com/youtube/v3/getting-started)). Put this key into variable DEVELOPER_KEY in the file app\src\main\java\yid\MasterClassLeanback\ui\VideoDetailsFragment.java.

Also you should create SQLite database with stored and categorised video URLs. The example of database and required strucure of database can be found [here](https://drive.google.com/open?id=0BzD8Oi0fi5O8MV9FMXpTVDNQMzg). You should place prepared database to the file hosting that supports direct link downloading and place that direct link in the file app\src\main\res\values\strings.xml as CatalogURL.

# License:
This application developed by Dmitrii Rashchenko and Iuliia Rashchenko in 2016. You can use it as template for your app without any restrictions.
Attribution for Yoda clip-art used on banner and logo: <a href="http://clipartmag.com/yoda-clipart-black-and-white">Yoda Clipart Black And White</a>. This image has CC BY-NC 4.0 Licence and is free for personal use with attribution.