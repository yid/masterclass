package yid.MasterClassLeanback.model;

import android.database.Cursor;
import android.support.v17.leanback.database.CursorMapper;

import yid.MasterClassLeanback.data.VideoContract;

public final class VideoCursorMapper extends CursorMapper {

    private static int URLIndex;
    private static int TitleIndex;
    private static int ChannelIndex;
    private static int DescriptionIndex;

    @Override
    protected void bindColumns(Cursor cursor) {
        URLIndex = cursor.getColumnIndex(VideoContract.VideoEntry.ColumnURL);
        TitleIndex = cursor.getColumnIndex(VideoContract.VideoEntry.ColumnTitle);
        ChannelIndex = cursor.getColumnIndex(VideoContract.VideoEntry.ColumnChannelTitle);
        DescriptionIndex = cursor.getColumnIndex(VideoContract.VideoEntry.ColumnDescription);
    }

    @Override
    protected Object bind(Cursor cursor) {
        String URL = cursor.getString(URLIndex);
        String Title = cursor.getString(TitleIndex);
        String ChannelTitle = cursor.getString(ChannelIndex);
        String Description = cursor.getString(DescriptionIndex);

        return new Video(Title,Description,URL,ChannelTitle);
    }
}
