package yid.MasterClassLeanback.model;

import android.os.Parcel;
import android.os.Parcelable;

public final class Video implements Parcelable {
    public final String title;
    public final String description;
    public final String videoUrl;
    public final String studio;

    Video(
            final String title,
            final String desc,
            final String videoUrl,
            final String studio) {
        this.title = title;
        this.description = desc;
        this.videoUrl = videoUrl;
        this.studio = studio;
    }

    private Video(Parcel in) {
        title = in.readString();
        description = in.readString();
        videoUrl = in.readString();
        studio = in.readString();
    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @Override
    public boolean equals(Object m) {
        return m instanceof Video && title == ((Video) m).title;
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(videoUrl);
        dest.writeString(studio);
    }

    @Override
    public String toString() {
        String s = "Video{";
        s += "title='" + title + "'";
        s += ", videoUrl='" + videoUrl + "'";
        s += ", studio='" + studio + "'";
        s += "}";
        return s;
    }
}
