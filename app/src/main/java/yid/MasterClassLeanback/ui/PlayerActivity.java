package yid.MasterClassLeanback.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

import yid.MasterClassLeanback.R;

public class PlayerActivity extends Activity {
    private PlayerFragment player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        player = (PlayerFragment)getFragmentManager().findFragmentById(R.id.playerFragment);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
               if(event.getAction()==KeyEvent.ACTION_DOWN){
            if(event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER ||
                    event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE ||
                    event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                player.PlayOrPause();
            }
            if(event.getKeyCode() == KeyEvent.KEYCODE_DPAD_LEFT ||
                    event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_REWIND){
                player.MoveBackward();
            }
            if(event.getKeyCode() == KeyEvent.KEYCODE_DPAD_RIGHT ||
                    event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD){
                player.MoveForward();
            }
        }

        return super.dispatchKeyEvent(event);
    }
}
