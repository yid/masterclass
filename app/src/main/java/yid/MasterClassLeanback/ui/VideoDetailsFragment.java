package yid.MasterClassLeanback.ui;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v17.leanback.app.DetailsFragment;
import android.support.v17.leanback.widget.Action;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.ClassPresenterSelector;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.DetailsOverviewLogoPresenter;
import android.support.v17.leanback.widget.DetailsOverviewRow;
import android.support.v17.leanback.widget.FullWidthDetailsOverviewRowPresenter;
import android.support.v17.leanback.widget.FullWidthDetailsOverviewSharedElementHelper;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnActionClickedListener;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.SparseArrayObjectAdapter;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import yid.MasterClassLeanback.R;

import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import yid.MasterClassLeanback.data.VideoContract;
import yid.MasterClassLeanback.model.Video;
import yid.MasterClassLeanback.model.VideoCursorMapper;
import yid.MasterClassLeanback.presenter.CardPresenter;
import yid.MasterClassLeanback.presenter.DetailsDescriptionPresenter;
import yid.MasterClassLeanback.presenter.ThumbnailToDrawableQueue;

public class VideoDetailsFragment extends DetailsFragment
        implements LoaderManager.LoaderCallbacks<Cursor>, OnActionClickedListener {
    private static final int ACTION_WATCH = 1;
    private static final int ACTION_ADD_TO_PREF = 2;
    private static final int RELATED_VIDEOS_LOADER = 1;
    private static final int PREFERENCES_CHECKER_LOADER = 2;
    public static final String DEVELOPER_KEY = "insert here your youtube developer key";
    private Action prefAction;


    private Video mSelectedVideo;
    private ArrayObjectAdapter mAdapter;
    private CursorObjectAdapter mVideoCursorAdapter;
    private final VideoCursorMapper mVideoCursorMapper = new VideoCursorMapper();
    private ThumbnailToDrawableQueue thumbnailToDrawableQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        thumbnailToDrawableQueue = new ThumbnailToDrawableQueue(getActivity());
        mVideoCursorAdapter = new CursorObjectAdapter(new CardPresenter(thumbnailToDrawableQueue));
        mVideoCursorAdapter.setMapper(mVideoCursorMapper);

        mSelectedVideo = getActivity().getIntent().getParcelableExtra(VideoDetailsActivity.VIDEO);

        if (mSelectedVideo != null) {
            setupAdapter();
            setupDetailsOverviewRow();
            setupMovieListRow();

            setOnItemViewClickedListener(new ItemViewClickedListener());
        }
        View v = super.onCreateView(inflater, container, savedInstanceState);
        v.setBackgroundColor(getResources().getColor(R.color.right_fragment_background));
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thumbnailToDrawableQueue.Release();
    }

    private void setupAdapter() {
        FullWidthDetailsOverviewRowPresenter detailsPresenter =
                new FullWidthDetailsOverviewRowPresenter(new DetailsDescriptionPresenter(),
                        new MovieDetailsOverviewLogoPresenter());

        detailsPresenter.setActionsBackgroundColor(getResources().getColor(R.color.default_background));
        detailsPresenter.setBackgroundColor(getResources().getColor(R.color.left_fragment_background));
        detailsPresenter.setInitialState(FullWidthDetailsOverviewRowPresenter.STATE_HALF);

        FullWidthDetailsOverviewSharedElementHelper mHelper = new FullWidthDetailsOverviewSharedElementHelper();
        mHelper.setSharedElementEnterTransition(getActivity(),VideoDetailsActivity.VIDEO);
        detailsPresenter.setListener(mHelper);
        detailsPresenter.setParticipatingEntranceTransition(false);
        prepareEntranceTransition();

        detailsPresenter.setOnActionClickedListener(this);

        ClassPresenterSelector mPresenterSelector = new ClassPresenterSelector();
        mPresenterSelector.addClassPresenter(DetailsOverviewRow.class, detailsPresenter);
        mPresenterSelector.addClassPresenter(ListRow.class, new ListRowPresenter());
        mAdapter = new ArrayObjectAdapter(mPresenterSelector);
        setAdapter(mAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if(id==RELATED_VIDEOS_LOADER) {
            return new CursorLoader(
                    getActivity(),
                    VideoContract.VideoEntry.CONTENT_CHANNELS_URI,
                    new String[]{VideoContract.VideoEntry.ColumnURL, VideoContract.VideoEntry._ID, VideoContract.VideoEntry.ColumnParent,
                            VideoContract.VideoEntry.ColumnChannelTitle, VideoContract.VideoEntry.ColumnTitle, VideoContract.VideoEntry.ColumnDescription},
                    VideoContract.VideoEntry.ColumnChannelTitle + " = ? AND " + VideoContract.VideoEntry.ColumnParent + " <> " + VideoContract.VideoEntry.PreferencesId,
                    new String[]{mSelectedVideo.studio},
                    VideoContract.VideoEntry.ColumnLikeCount
            );
        }else {
            return new CursorLoader(
                    getActivity(),
                    VideoContract.VideoEntry.CONTENT_CHANNELS_URI,
                    new String[]{VideoContract.VideoEntry._ID},
                    VideoContract.VideoEntry.ColumnParent + " = " + VideoContract.VideoEntry.PreferencesId + " AND " + VideoContract.VideoEntry.ColumnURL + " = ?",
                    new String[]{mSelectedVideo.videoUrl},
                    null
            );
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(loader.getId()==RELATED_VIDEOS_LOADER) {
            if (cursor != null && cursor.moveToNext())
                mVideoCursorAdapter.changeCursor(cursor);
            mAdapter.notifyArrayItemRangeChanged(0,1);
        }
        else{
            if(cursor.moveToFirst()){
                prefAction.setLabel1(getResources().getString(R.string.RemoveFromPreferences1));
                prefAction.setLabel2(getResources().getString(R.string.RemoveFromPreferences2));
            }
            else{
                prefAction.setLabel1(getResources().getString(R.string.AddToPreferences1));
                prefAction.setLabel2(getResources().getString(R.string.AddToPreferences2));
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mVideoCursorAdapter.changeCursor(null);
    }

    @Override
    public void onActionClicked(Action action) {
        if (action.getId() == ACTION_WATCH) {
            Intent intent = new Intent(getActivity(),PlayerActivity.class);
            intent.putExtra(VideoDetailsActivity.VIDEO,mSelectedVideo);
            startActivity(intent);
        } else {
            if(action.getLabel1() == getResources().getString(R.string.RemoveFromPreferences1)){
                action.setLabel1(getResources().getString(R.string.LoadingPreferences1));
                action.setLabel2(getResources().getString(R.string.LoadingPreferences2));
                getActivity().getContentResolver().
                        delete(VideoContract.VideoEntry.CONTENT_CHANNELS_URI,
                                VideoContract.VideoEntry.ColumnParent + " = " + VideoContract.VideoEntry.PreferencesId + " AND " + VideoContract.VideoEntry.ColumnURL + " = ?",
                                new String[]{mSelectedVideo.videoUrl});
                getLoaderManager().restartLoader(PREFERENCES_CHECKER_LOADER,null,this);
            }
            if(action.getLabel1() == getResources().getString(R.string.AddToPreferences1)){
                action.setLabel1(getResources().getString(R.string.LoadingPreferences1));
                action.setLabel2(getResources().getString(R.string.LoadingPreferences2));
                ContentValues cv = new ContentValues();
                cv.put(VideoContract.VideoEntry.ColumnParent, VideoContract.VideoEntry.PreferencesId);
                cv.put(VideoContract.VideoEntry.ColumnURL, mSelectedVideo.videoUrl);
                cv.put(VideoContract.VideoEntry.ColumnDescription, mSelectedVideo.description);
                cv.put(VideoContract.VideoEntry.ColumnTitle, mSelectedVideo.title);
                cv.put(VideoContract.VideoEntry.ColumnChannelTitle,mSelectedVideo.studio);
                cv.put(VideoContract.VideoEntry.ColumnLikeCount,0);
                getActivity().getContentResolver().insert(VideoContract.VideoEntry.CONTENT_CHANNELS_URI, cv);
                getLoaderManager().restartLoader(PREFERENCES_CHECKER_LOADER,null,this);
            }
        }
    }

    static class MovieDetailsOverviewLogoPresenter extends DetailsOverviewLogoPresenter {

        static class ViewHolder extends DetailsOverviewLogoPresenter.ViewHolder {
            ViewHolder(View view) {
                super(view);
            }

            public FullWidthDetailsOverviewRowPresenter getParentPresenter() {
                return mParentPresenter;
            }

            public FullWidthDetailsOverviewRowPresenter.ViewHolder getParentViewHolder() {
                return mParentViewHolder;
            }
        }

        @Override
        public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
            ImageView imageView = (ImageView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lb_fullwidth_details_overview_logo, parent, false);

            Resources res = parent.getResources();
            int width = res.getDimensionPixelSize(R.dimen.detail_thumb_width);
            int height = res.getDimensionPixelSize(R.dimen.detail_thumb_height);
            imageView.setLayoutParams(new ViewGroup.MarginLayoutParams(width, height));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            return new ViewHolder(imageView);
        }

        @Override
        public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
            DetailsOverviewRow row = (DetailsOverviewRow) item;
            ImageView imageView = ((ImageView) viewHolder.view);
            imageView.setImageDrawable(row.getImageDrawable());
            if (isBoundToImage((ViewHolder) viewHolder, row)) {
                MovieDetailsOverviewLogoPresenter.ViewHolder vh =
                        (MovieDetailsOverviewLogoPresenter.ViewHolder) viewHolder;
                vh.getParentPresenter().notifyOnBindLogo(vh.getParentViewHolder());
            }
        }
    }

    private void setupDetailsOverviewRow() {
        final DetailsOverviewRow row = new DetailsOverviewRow(mSelectedVideo);
        thumbnailToDrawableQueue.AddToQueue(mSelectedVideo.videoUrl, new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                row.setImageDrawable(youTubeThumbnailView.getDrawable());
                startEntranceTransition();
            }

            @Override
            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                startEntranceTransition();
            }
        });

        SparseArrayObjectAdapter adapter = new SparseArrayObjectAdapter();

        Drawable iconPlay = getResources().getDrawable(R.drawable.play);
        iconPlay.setColorFilter(getResources().getColor(R.color.right_fragment_background), PorterDuff.Mode.SRC_IN);

        Drawable iconPref = getResources().getDrawable(R.drawable.preferences);
        iconPref.setColorFilter(getResources().getColor(R.color.right_fragment_background), PorterDuff.Mode.SRC_IN);

        prefAction = new Action(ACTION_ADD_TO_PREF, getResources().getString(R.string.LoadingPreferences1), getResources().getString(R.string.LoadingPreferences2), iconPref);
        getLoaderManager().initLoader(PREFERENCES_CHECKER_LOADER,null,this);

        adapter.set(ACTION_WATCH, new Action(ACTION_WATCH, getResources().getString(R.string.WatchVideo1), getResources().getString(R.string.WatchVideo2), iconPlay));
        adapter.set(ACTION_ADD_TO_PREF, prefAction);
        row.setActionsAdapter(adapter);

        mAdapter.add(row);
    }

    private void setupMovieListRow() {
        String subcategories[] = {getResources().getString(R.string.related_videos)};

        getLoaderManager().initLoader(RELATED_VIDEOS_LOADER, null, this);

        HeaderItem header = new HeaderItem(0, subcategories[0]);
        mAdapter.add(new ListRow(header, mVideoCursorAdapter));
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Video) {
                Video video = (Video) item;
                Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        ((ImageCardView) itemViewHolder.view).getMainImageView(),
                        VideoDetailsActivity.VIDEO).toBundle();
                getActivity().startActivity(intent, bundle);
            }
        }
    }
}
