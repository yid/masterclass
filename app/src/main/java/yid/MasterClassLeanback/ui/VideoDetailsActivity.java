package yid.MasterClassLeanback.ui;

import android.app.Activity;
import android.os.Bundle;

import yid.MasterClassLeanback.R;

public class VideoDetailsActivity extends Activity {
    public static final String VIDEO = "Video";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_details);
    }
}
