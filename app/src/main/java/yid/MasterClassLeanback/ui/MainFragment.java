package yid.MasterClassLeanback.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.app.HeadersFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.PresenterSelector;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowHeaderPresenter;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;

import yid.MasterClassLeanback.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import yid.MasterClassLeanback.data.VideoContract;
import yid.MasterClassLeanback.model.Video;
import yid.MasterClassLeanback.model.VideoCursorMapper;
import yid.MasterClassLeanback.presenter.CardPresenter;
import yid.MasterClassLeanback.presenter.GridItemPresenter;
import yid.MasterClassLeanback.presenter.IconHeaderItemPresenter;
import yid.MasterClassLeanback.presenter.ThumbnailToDrawableQueue;

public class MainFragment extends BrowseFragment implements LoaderManager.LoaderCallbacks<Cursor>, HeadersFragment.OnHeaderClickedListener, OnItemViewClickedListener, View.OnClickListener {
    private ArrayObjectAdapter CategoryRowAdapter;
    private static final int CATEGORY_LOADER = 0;
    private Stack<Integer> StackOfParents ;
    private ThumbnailToDrawableQueue thumbnailToDrawableQueue;
    private Map<Integer, CursorObjectAdapter> VideoCursorAdapters;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        VideoCursorAdapters = new HashMap<>();
        StackOfParents = new Stack<>();
        StackOfParents.push(0);
        thumbnailToDrawableQueue = new ThumbnailToDrawableQueue(getActivity());
        CategoryRowAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        setAdapter(CategoryRowAdapter);
        getLoaderManager().initLoader(CATEGORY_LOADER, null, this);
        setupUIElements();
        setupEventListeners();
    }

    private void setupUIElements() {
        prepareEntranceTransition();
        setHeadersState(HEADERS_ENABLED);
        setHeadersTransitionOnBackEnabled(true);

        getView().setBackgroundColor(getResources().getColor(R.color.right_fragment_background));
        setBadgeDrawable(getActivity().getResources().getDrawable(R.drawable.banner));
        setBrandColor(getResources().getColor(R.color.left_fragment_background));
        setSearchAffordanceColor(getResources().getColor(R.color.video_selected_background));

        setHeaderPresenterSelector(new PresenterSelector() {
            @Override
            public Presenter getPresenter(Object o) {
                return new IconHeaderItemPresenter();
            }
        });
    }

    private void setupEventListeners() {
        setOnSearchClickedListener(this);
        setOnItemViewClickedListener(this);
        getHeadersFragment().setOnHeaderClickedListener(this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == CATEGORY_LOADER) {
            return new CursorLoader(
                    getActivity(),
                    VideoContract.VideoEntry.CONTENT_CATEGORIES_URI,
                    new String[]{VideoContract.VideoEntry.ColumnName, VideoContract.VideoEntry._ID, VideoContract.VideoEntry.ColumnLastLevel},
                    VideoContract.VideoEntry.ColumnParent + " = ?",
                    new String[]{Integer.toString(StackOfParents.peek())},
                    VideoContract.VideoEntry._ID
            );
        } else {
            if(args.getBoolean(VideoContract.VideoEntry.ColumnLastLevel)){
                return new CursorLoader(
                        getActivity(),
                        VideoContract.VideoEntry.CONTENT_CHANNELS_URI,
                        new String[]{VideoContract.VideoEntry.ColumnURL, VideoContract.VideoEntry._ID, VideoContract.VideoEntry.ColumnParent,
                                VideoContract.VideoEntry.ColumnChannelTitle, VideoContract.VideoEntry.ColumnTitle, VideoContract.VideoEntry.ColumnDescription},
                        VideoContract.VideoEntry.ColumnParent + " = ?",
                        new String[]{Integer.toString(id)},
                        VideoContract.VideoEntry.ColumnLikeCount
                );
            }
            else {
                int CategoryLevel;
                for (CategoryLevel = VideoContract.VideoEntry.MainCategorySize; CategoryLevel > 0; CategoryLevel /= 10) {
                    if (id % CategoryLevel == 0)
                        break;
                }
                return new CursorLoader(
                        getActivity(),
                        VideoContract.VideoEntry.CONTENT_CHANNELS_LIMITED_URI,
                        new String[]{VideoContract.VideoEntry.ColumnURL, VideoContract.VideoEntry._ID, VideoContract.VideoEntry.ColumnParent,
                                VideoContract.VideoEntry.ColumnChannelTitle, VideoContract.VideoEntry.ColumnTitle, VideoContract.VideoEntry.ColumnDescription},
                        VideoContract.VideoEntry._ID + " BETWEEN ? AND ?",
                        new String[]{Integer.toString(id), Integer.toString(id + CategoryLevel)},
                        VideoContract.VideoEntry.ColumnLikeCount
                );
            }
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        final int loaderId = loader.getId();

        if (loaderId == CATEGORY_LOADER) {
            CategoryRowAdapter.clear();
            VideoCursorAdapters.clear();
            if(data.getCount() > 1) {
                if (data.moveToFirst()) {
                    do {
                        String category = data.getString(data.getColumnIndex(VideoContract.VideoEntry.ColumnName));
                        HeaderItem header = new HeaderItem(category);
                        boolean LastLevel = false;
                        if (data.getInt(data.getColumnIndex(VideoContract.VideoEntry.ColumnLastLevel)) > 0) {
                            LastLevel = true;
                            header.setContentDescription(VideoContract.VideoEntry.ColumnLastLevel);
                        }

                        int CategoryId = data.getInt(data.getColumnIndex(VideoContract.VideoEntry._ID));
                        CursorObjectAdapter videoCursorAdapter = new CursorObjectAdapter(new CardPresenter(thumbnailToDrawableQueue));
                        videoCursorAdapter.setMapper(new VideoCursorMapper());
                        VideoCursorAdapters.put(CategoryId, videoCursorAdapter);

                        ListRow row = new ListRow(CategoryId, header, videoCursorAdapter);
                        CategoryRowAdapter.add(row);

                        Bundle args = new Bundle();
                        args.putInt(VideoContract.VideoEntry._ID, CategoryId);
                        args.putBoolean(VideoContract.VideoEntry.ColumnLastLevel, LastLevel);

                        getLoaderManager().initLoader(CategoryId, args, this);

                    } while (data.moveToNext());
                }
            }else{
                HeaderItem gridHeader = new HeaderItem(getResources().getString(R.string.ErrorMessage1));
                GridItemPresenter gridPresenter = new GridItemPresenter();
                ArrayObjectAdapter gridRowAdapter = new ArrayObjectAdapter(gridPresenter);
                gridRowAdapter.add(getResources().getString(R.string.SuggestedSolution1));
                ListRow row = new ListRow(-1,gridHeader, gridRowAdapter);
                CategoryRowAdapter.add(row);
            }
        } else {
            CursorObjectAdapter videoCursorAdapter = VideoCursorAdapters.get(loaderId);
            if(videoCursorAdapter!=null)
                videoCursorAdapter.changeCursor(data);
        }

        startEntranceTransition();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    @Override
    public void onHeaderClicked(RowHeaderPresenter.ViewHolder viewHolder, Row row) {
        if(row.getId()>0 && row.getHeaderItem().getContentDescription() != VideoContract.VideoEntry.ColumnLastLevel){
            StackOfParents.push((int)row.getId());
            getLoaderManager().restartLoader(CATEGORY_LOADER, null, this);
        }
    }

    @Override
    public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item, RowPresenter.ViewHolder rowViewHolder, Row row) {
        if (item instanceof Video) {
            Video video = (Video) item;
            Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
            intent.putExtra(VideoDetailsActivity.VIDEO, video);

            Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(),
                    ((ImageCardView) itemViewHolder.view).getMainImageView(),
                    VideoDetailsActivity.VIDEO).toBundle();

            getActivity().startActivity(intent, bundle);
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        startActivity(intent);
    }

    public boolean onBackPressed() {
        if (StackOfParents.peek() != 0 && isShowingHeaders()) {
            StackOfParents.pop();
            getLoaderManager().restartLoader(CATEGORY_LOADER, null, this);
            return false;
        }
        return true;
    }
}
