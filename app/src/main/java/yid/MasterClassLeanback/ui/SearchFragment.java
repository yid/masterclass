package yid.MasterClassLeanback.ui;


import android.app.Activity;
import android.app.LoaderManager;
import android.content.ActivityNotFoundException;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.CursorObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.ObjectAdapter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v17.leanback.widget.SpeechRecognitionCallback;
import android.support.v4.app.ActivityOptionsCompat;
import android.text.TextUtils;
import android.util.Log;

import yid.MasterClassLeanback.R;

import yid.MasterClassLeanback.data.VideoContract;
import yid.MasterClassLeanback.model.Video;
import yid.MasterClassLeanback.model.VideoCursorMapper;
import yid.MasterClassLeanback.presenter.CardPresenter;
import yid.MasterClassLeanback.presenter.ThumbnailToDrawableQueue;


public class SearchFragment extends android.support.v17.leanback.app.SearchFragment
        implements android.support.v17.leanback.app.SearchFragment.SearchResultProvider,
        LoaderManager.LoaderCallbacks<Cursor>, SpeechRecognitionCallback {
    private static final String TAG = "SearchFragment";
    private static final int REQUEST_SPEECH = 0x00000010;

    private final Handler mHandler = new Handler();
    private ArrayObjectAdapter mRowsAdapter;
    private String mQuery;
    private CursorObjectAdapter mVideoCursorAdapter;

    private int mSearchLoaderId = 1;
    private boolean mResultsFound = false;

    private ThumbnailToDrawableQueue thumbnailToDrawableQueue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        thumbnailToDrawableQueue = new ThumbnailToDrawableQueue(getActivity());
        mVideoCursorAdapter = new CursorObjectAdapter(new CardPresenter(thumbnailToDrawableQueue));
        mVideoCursorAdapter.setMapper(new VideoCursorMapper());
        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());

        setSpeechRecognitionCallback(this);
        setSearchResultProvider(this);
        setOnItemViewClickedListener(new ItemViewClickedListener());
    }

    @Override
    public void onPause() {
        mHandler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thumbnailToDrawableQueue.Release();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_SPEECH){
            if(resultCode == Activity.RESULT_OK)
                setSearchQuery(data, true);
            else
            if (!hasResults()) {
                getView().findViewById(R.id.lb_search_bar_speech_orb).requestFocus();
            }
        }
    }

    @Override
    public ObjectAdapter getResultsAdapter() {
        return mRowsAdapter;
    }

    @Override
    public boolean onQueryTextChange(String newQuery) {
        loadQuery(newQuery);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        loadQuery(query);
        return true;
    }

    public boolean hasResults() {
        return mRowsAdapter.size() > 0 && mResultsFound;
    }

    private void loadQuery(String query) {
        if (!TextUtils.isEmpty(query) && !query.equals("nil")) {
            mQuery = query;
            getLoaderManager().initLoader(mSearchLoaderId++, null, this);
        }
    }

    public void focusOnSearch() {
        getView().findViewById(R.id.lb_search_bar).requestFocus();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String query = mQuery;
        return new CursorLoader(
                getActivity(),
                VideoContract.VideoEntry.CONTENT_CHANNELS_URI,
                new String[]{VideoContract.VideoEntry.ColumnURL, VideoContract.VideoEntry._ID, VideoContract.VideoEntry.ColumnParent,
                        VideoContract.VideoEntry.ColumnChannelTitle, VideoContract.VideoEntry.ColumnTitle, VideoContract.VideoEntry.ColumnDescription},
                VideoContract.VideoEntry.ColumnParent + " <> " + VideoContract.VideoEntry.PreferencesId + " AND (" + VideoContract.VideoEntry.ColumnChannelTitle + " LIKE ? OR " + VideoContract.VideoEntry.ColumnDescription + " LIKE ? OR " + VideoContract.VideoEntry.ColumnTitle + " LIKE ?)",
                new String[]{"%" + query + "%","%" + query + "%","%" + query + "%"},
                VideoContract.VideoEntry.ColumnLikeCount
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        int titleRes;
        if (cursor != null && cursor.moveToFirst()) {
            mResultsFound = true;
            titleRes = R.string.search_results;
        } else {
            mResultsFound = false;
            titleRes = R.string.no_search_results;
        }
        mVideoCursorAdapter.changeCursor(cursor);
        HeaderItem header = new HeaderItem(getString(titleRes, mQuery));
        mRowsAdapter.clear();
        ListRow row = new ListRow(header, mVideoCursorAdapter);
        mRowsAdapter.add(row);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mVideoCursorAdapter.changeCursor(null);
    }

    @Override
    public void recognizeSpeech() {
        try {
            startActivityForResult(getRecognizerIntent(), REQUEST_SPEECH);
        } catch (ActivityNotFoundException e) {
            focusOnSearch();
            Log.e(TAG, "Cannot find activity for speech recognizer", e);
        }
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Video) {
                Video video = (Video) item;
                Intent intent = new Intent(getActivity(), VideoDetailsActivity.class);
                intent.putExtra(VideoDetailsActivity.VIDEO, video);

                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        ((ImageCardView) itemViewHolder.view).getMainImageView(),
                        VideoDetailsActivity.VIDEO).toBundle();
                getActivity().startActivity(intent, bundle);
            }
        }
    }

}
