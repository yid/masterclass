package yid.MasterClassLeanback.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;

import yid.MasterClassLeanback.R;

public class MainActivity extends Activity {
    private MainFragment mainFragment = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mainFragment = (MainFragment)getFragmentManager().findFragmentById(R.id.main_frame);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(KeyEvent.KEYCODE_DPAD_LEFT == keyCode)
            mainFragment.onBackPressed();
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if(mainFragment.onBackPressed())
            super.onBackPressed();
    }
}
