package yid.MasterClassLeanback.ui;

import android.os.Bundle;

import yid.MasterClassLeanback.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import yid.MasterClassLeanback.model.Video;

public class PlayerFragment extends YouTubePlayerFragment implements YouTubePlayer.OnInitializedListener {
    private YouTubePlayer player = null;
    private String VideoId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(VideoDetailsFragment.DEVELOPER_KEY, this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Video video = getActivity().getIntent().getParcelableExtra(VideoDetailsActivity.VIDEO);
        VideoId = video.videoUrl;
    }

    @Override
    public void onDestroy() {
        if (player != null) {
            player.release();
        }
        super.onDestroy();
    }

    public void PlayOrPause() {
        if (player != null) {
            if(player.isPlaying()) {
                player.pause();
                this.player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            }
            else {
                player.play();
                this.player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
            }
        }
    }

    public void MoveForward() {
        if (player != null) {
            player.seekRelativeMillis(getResources().getInteger(R.integer.PlayerMoveMillis));
        }
    }

    public void MoveBackward() {
        if (player != null) {
            player.seekRelativeMillis(-1 * getResources().getInteger(R.integer.PlayerMoveMillis));
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean restored) {
        this.player = player;
        this.player.setShowFullscreenButton(false);
        if (!restored) {
            player.cueVideo(VideoId);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        this.player = null;
    }
}
