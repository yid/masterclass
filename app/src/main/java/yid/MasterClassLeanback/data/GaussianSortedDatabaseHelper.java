package yid.MasterClassLeanback.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GaussianSortedDatabaseHelper extends SQLiteOpenHelper {
    public GaussianSortedDatabaseHelper(Context context){
        super(context, "GaussianSorted", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + VideoContract.VideoEntry.TableChannels + " (" + VideoContract.VideoEntry._ID + " INTEGER PRIMARY KEY, "
                + VideoContract.VideoEntry.ColumnTitle + " TEXT, " + VideoContract.VideoEntry.ColumnChannelTitle + " TEXT, " + VideoContract.VideoEntry.ColumnLikeCount + " INTEGER, "
                + VideoContract.VideoEntry.ColumnDescription + " TEXT, "
                + VideoContract.VideoEntry.ColumnParent + " INTEGER, " + VideoContract.VideoEntry.ColumnURL + " TEXT, FOREIGN KEY ("
                + VideoContract.VideoEntry.ColumnParent + ") REFERENCES " + VideoContract.VideoEntry.TableCategories + "(" + VideoContract.VideoEntry._ID + "));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
