package yid.MasterClassLeanback.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import yid.MasterClassLeanback.R;
import yid.MasterClassLeanback.data.VideoContract.VideoEntry;

class VideoDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private final Context context;
    private final String TempDbPath;
    private static final String SharedPreferencesTag = "DatabaseReady";
    private static final String DATABASE_NAME = "Catalog.s3db";
    private static final String TEMP_DATABASE_NAME = "TempCatalog.s3db";
    private static  final String TAG = "DBHelperErrorTAG";

    VideoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        TempDbPath = context.getApplicationInfo().dataDir + "/" + TEMP_DATABASE_NAME;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + VideoEntry.TableCategories + "(" + VideoEntry._ID + " INTEGER PRIMARY KEY, "
                + VideoEntry.ColumnParent + " INTEGER, " + VideoEntry.ColumnName + " TEXT, " + VideoEntry.ColumnLastLevel + " BOOLEAN);");
        db.execSQL("CREATE TABLE " + VideoEntry.TableChannels + " (" + VideoEntry._ID + " INTEGER PRIMARY KEY, "
                + VideoEntry.ColumnTitle + " TEXT, " + VideoEntry.ColumnChannelTitle + " TEXT, " + VideoEntry.ColumnLikeCount + " INTEGER, "
                + VideoEntry.ColumnDescription + " TEXT, "
                + VideoEntry.ColumnParent + " INTEGER, " + VideoEntry.ColumnURL + " TEXT, FOREIGN KEY ("
                + VideoEntry.ColumnParent + ") REFERENCES " + VideoEntry.TableCategories + "(" + VideoEntry._ID + "));");

        ContentValues cv = new ContentValues();
        cv.put(VideoEntry._ID, VideoEntry.PreferencesId);
        cv.put(VideoEntry.ColumnParent, 0);
        cv.put(VideoEntry.ColumnName, context.getResources().getString(R.string.preferencesName));
        cv.put(VideoEntry.ColumnLastLevel, true);
        db.insert(VideoEntry.TableCategories,null,cv);
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        SharedPreferences sp = context.getSharedPreferences(SharedPreferencesTag, Context.MODE_PRIVATE);
        if(!sp.getBoolean(SharedPreferencesTag, false))
            if(LoadDatabase()){
                SQLiteDatabase db = getWritableDatabase();
                if(ReplaceDatabases(db)){
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putBoolean(SharedPreferencesTag,true);
                    editor.apply();
                }
                db.close();
            }
        return super.getReadableDatabase();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    private boolean LoadDatabase() {
        InputStream input = null;
        FileOutputStream output = null;
        URL url;
        HttpURLConnection Connection = null;
        try {
            url = new URL(context.getResources().getString(R.string.CatalogURL));
            Connection = (HttpURLConnection)url.openConnection();
            input = Connection.getInputStream();
            output = new FileOutputStream(TempDbPath);
            byte[] buffer = new byte[1024];
            int len1;
            while ((len1 = input.read(buffer)) > 0)
                output.write(buffer, 0, len1);
            return true;
        }catch (IOException e){
            Log.e(TAG, "Error occurred in downloading videos");
            return false;
        }
        finally {
            if (Connection != null)
                Connection.disconnect();
            try {
                if (input != null)
                    input.close();
                if (output != null)
                    output.close();
            }catch (IOException e2){
                Log.e(TAG, "Can't close some files");
            }
        }
    }

    private boolean ReplaceDatabases(SQLiteDatabase db) {
        SQLiteDatabase dbTemp = null;
        try {
            dbTemp = SQLiteDatabase.openDatabase(TempDbPath, null, SQLiteDatabase.OPEN_READWRITE);
        }catch (Exception e){
            if(dbTemp!=null)
                dbTemp.close();
            return false;
        }

        Cursor c = dbTemp.query(VideoEntry.TableChannels, null, null, null, null, null, null);

        db.delete(VideoEntry.TableCategories, VideoEntry._ID + " <> " + VideoEntry.PreferencesId, null);
        db.delete(VideoEntry.TableChannels, VideoEntry.ColumnParent + " <> " + VideoEntry.PreferencesId, null);

        ContentValues cv = new ContentValues();
        if (c.moveToFirst())
            do{
                cv.clear();
                cv.put(VideoEntry._ID, c.getInt(c.getColumnIndex(VideoEntry._ID)));
                cv.put(VideoEntry.ColumnTitle, c.getString(c.getColumnIndex(VideoEntry.ColumnTitle)));
                cv.put(VideoEntry.ColumnChannelTitle, c.getString(c.getColumnIndex(VideoEntry.ColumnChannelTitle)));
                cv.put(VideoEntry.ColumnDescription, c.getString(c.getColumnIndex(VideoEntry.ColumnDescription)));
                cv.put(VideoEntry.ColumnLikeCount, c.getInt(c.getColumnIndex(VideoEntry.ColumnLikeCount)));
                cv.put(VideoEntry.ColumnParent, c.getInt(c.getColumnIndex(VideoEntry.ColumnParent)));
                cv.put(VideoEntry.ColumnURL, c.getString(c.getColumnIndex(VideoEntry.ColumnURL)));
                db.insert(VideoEntry.TableChannels,null,cv);
            }while(c.moveToNext());
        c.close();
        c = dbTemp.query(VideoEntry.TableCategories,null,null,null,null,null,null);
        if (c.moveToFirst())
            do{
                cv.clear();
                cv.put(VideoEntry._ID, c.getInt(c.getColumnIndex(VideoEntry._ID)));
                cv.put(VideoEntry.ColumnParent, c.getInt(c.getColumnIndex(VideoEntry.ColumnParent)));
                cv.put(VideoEntry.ColumnName, c.getString(c.getColumnIndex(VideoEntry.ColumnName)));
                boolean b = false;
                if(c.getInt(c.getColumnIndex(VideoEntry.ColumnLastLevel))>0)
                    b=true;
                cv.put(VideoEntry.ColumnLastLevel, b);
                db.insert(VideoEntry.TableCategories,null,cv);
            }while(c.moveToNext());
        c.close();
        dbTemp.close();
        return true;
    }
}
