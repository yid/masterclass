package yid.MasterClassLeanback.data;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.Arrays;
import java.util.Random;

import yid.MasterClassLeanback.R;

public class VideoProvider extends ContentProvider {

    private static final int URI_CATEGORIES = 1;
    private static final int URI_CHANNELS = 3;
    private static final int URI_CHANNELS_LIMITED = 4;
    private static final String PreferencesLast = "PreferencesLast";

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(VideoContract.CONTENT_AUTHORITY, VideoContract.PATH_CATEGORIES, URI_CATEGORIES);
        uriMatcher.addURI(VideoContract.CONTENT_AUTHORITY, VideoContract.PATH_CHANNELS, URI_CHANNELS);
        uriMatcher.addURI(VideoContract.CONTENT_AUTHORITY, VideoContract.PATH_CHANNELS_LIMITED, URI_CHANNELS_LIMITED);
    }

    private VideoDbHelper mOpenHelper;
    private ContentResolver mContentResolver;
    private GaussianSortedDatabaseHelper gaussianSortedDatabaseHelper;

    @Override
    public boolean onCreate() {
        Context context = getContext();
        if (context != null)
            mContentResolver = context.getContentResolver();
        mOpenHelper = new VideoDbHelper(context);
        gaussianSortedDatabaseHelper = new GaussianSortedDatabaseHelper(context);
        return true;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch (uriMatcher.match(uri)) {
            case URI_CATEGORIES:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        VideoContract.VideoEntry.TableCategories,
                        projection,selection,selectionArgs,null,null,sortOrder);
                break;
            case URI_CHANNELS:
                retCursor = mOpenHelper.getReadableDatabase().query(
                        VideoContract.VideoEntry.TableChannels,
                        projection,selection,selectionArgs,null,null,sortOrder);
                break;
            case URI_CHANNELS_LIMITED:
                Cursor c = mOpenHelper.getReadableDatabase().query(
                        VideoContract.VideoEntry.TableChannels,
                        null,
                        selection,selectionArgs,null,null,sortOrder
                        );
                SQLiteDatabase db = gaussianSortedDatabaseHelper.getWritableDatabase();
                db.delete(VideoContract.VideoEntry.TableChannels, selection, selectionArgs);

                ContentValues cv = new ContentValues();
                int[] randomIndexes = GenerateGaussian(getContext().getResources().getInteger(R.integer.MinThumbnailsNum),c.getCount());
                for(int i=0;i<randomIndexes.length;i++){
                    c.moveToPosition(randomIndexes[i]);
                    cv.clear();
                    cv.put(VideoContract.VideoEntry._ID, c.getInt(c.getColumnIndex(VideoContract.VideoEntry._ID)));
                    cv.put(VideoContract.VideoEntry.ColumnTitle, c.getString(c.getColumnIndex(VideoContract.VideoEntry.ColumnTitle)));
                    cv.put(VideoContract.VideoEntry.ColumnChannelTitle, c.getString(c.getColumnIndex(VideoContract.VideoEntry.ColumnChannelTitle)));
                    cv.put(VideoContract.VideoEntry.ColumnDescription, c.getString(c.getColumnIndex(VideoContract.VideoEntry.ColumnDescription)));
                    cv.put(VideoContract.VideoEntry.ColumnLikeCount, c.getInt(c.getColumnIndex(VideoContract.VideoEntry.ColumnLikeCount)));
                    cv.put(VideoContract.VideoEntry.ColumnParent, c.getInt(c.getColumnIndex(VideoContract.VideoEntry.ColumnParent)));
                    cv.put(VideoContract.VideoEntry.ColumnURL, c.getString(c.getColumnIndex(VideoContract.VideoEntry.ColumnURL)));
                    db.insert(VideoContract.VideoEntry.TableChannels,null,cv);
                }
                c.close();
                retCursor = db.query(VideoContract.VideoEntry.TableChannels,projection,selection,selectionArgs,null,null,null);
                break;
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        retCursor.setNotificationUri(mContentResolver, uri);
        return retCursor;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case URI_CATEGORIES:
                return VideoContract.VideoEntry.CONTENT_CATEGORIES_TYPE;
            case URI_CHANNELS:
                return VideoContract.VideoEntry.CONTENT_CHANNELS_TYPE;
            case URI_CHANNELS_LIMITED:
                return VideoContract.VideoEntry.CONTENT_CHANNELS_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {

        switch (uriMatcher.match(uri)) {
            case URI_CHANNELS: {
                SharedPreferences sp = getContext().getSharedPreferences(PreferencesLast,Context.MODE_PRIVATE);
                int LastPreference = sp.getInt(PreferencesLast, VideoContract.VideoEntry.PreferencesId + 1);
                values.put(VideoContract.VideoEntry._ID, LastPreference);
                long _id = mOpenHelper.getWritableDatabase().insert(
                        VideoContract.VideoEntry.TableChannels, null, values);
                if (_id <= 0) { throw new SQLException("Failed to insert row into " + uri);}
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt(PreferencesLast, LastPreference + 1);
                editor.apply();
                break;
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
        mContentResolver.notifyChange(uri, null);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final int rowsDeleted;

        if (selection == null) {
            throw new UnsupportedOperationException("Cannot delete without selection specified.");
        }

        switch (uriMatcher.match(uri)) {
            case URI_CHANNELS:
                rowsDeleted = mOpenHelper.getWritableDatabase().delete(
                        VideoContract.VideoEntry.TableChannels, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsDeleted != 0) {
            mContentResolver.notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public static int[] GenerateGaussian(int nNeeded, int nTotal){
        Random r = new Random();
        if (nNeeded > nTotal)
            nNeeded = nTotal;
        double mu = 0;
        double sigma = nTotal / 3;
        int [] result = new int[nNeeded];
        Arrays.fill(result, -1);
        boolean success = false;
        int val = -1;
        for (int i = 0; i < nNeeded; i++){
            while(!success){
                val = Math.abs((int)(r.nextGaussian() * sigma + mu));
                if (val < nTotal){
                    success = true;
                    for (int j = 0; j < i; j++){
                        if (result[j] == val){
                            success = false;
                            break;
                        }
                    }
                }
            }
            result[i] = val;
            success = false;
        }
        return result;
    }
}
