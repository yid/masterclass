package yid.MasterClassLeanback.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class VideoContract {

    static final String CONTENT_AUTHORITY = "yid.MasterClassLeanback";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    static final String PATH_CATEGORIES = "categories";
    static final String PATH_CHANNELS = "channels";
    static final String PATH_CHANNELS_LIMITED = "limited";

    public static final class VideoEntry implements BaseColumns {

        public static final Uri CONTENT_CATEGORIES_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CATEGORIES).build();
        public static final Uri CONTENT_CHANNELS_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHANNELS).build();
        public static final Uri CONTENT_CHANNELS_LIMITED_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CHANNELS_LIMITED).build();
        static final String CONTENT_CATEGORIES_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CATEGORIES;
        static final String CONTENT_CHANNELS_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "." + PATH_CHANNELS;

        static final String TableCategories = "Categories";
        static final String TableChannels = "Channels";
        public static final String ColumnParent = "ParentId";
        public static final String ColumnName = "Name";
        public static final String ColumnLastLevel = "LastLevel";
        public static final String ColumnURL = "URL";
        public static final String ColumnTitle = "Title";
        public static final String ColumnChannelTitle = "ChannelTitle";
        public static final String ColumnLikeCount = "LikeCount";
        public static final String ColumnDescription = "Description";

        public static final int PreferencesId = 10000000;
        public static final int MainCategorySize = 100000000;
    }
}
