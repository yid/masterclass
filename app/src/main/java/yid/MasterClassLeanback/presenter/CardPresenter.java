package yid.MasterClassLeanback.presenter;

import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.Presenter;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import yid.MasterClassLeanback.R;
import yid.MasterClassLeanback.model.Video;

public class CardPresenter extends Presenter {
    private int mSelectedBackgroundColor = -1;
    private int mDefaultBackgroundColor = -1;
    private final ThumbnailToDrawableQueue thumbnailToDrawableQueue;

    public CardPresenter(ThumbnailToDrawableQueue thumbnailToDrawableQueue){
        this.thumbnailToDrawableQueue = thumbnailToDrawableQueue;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        mDefaultBackgroundColor = parent.getContext().getResources().getColor(R.color.default_background);
        mSelectedBackgroundColor = parent.getContext().getResources().getColor(R.color.video_selected_background);

        ImageCardView cardView = new ImageCardView(parent.getContext()) {
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };

        cardView.setFocusable(true);
        cardView.setFocusableInTouchMode(true);
        updateCardBackgroundColor(cardView, false);
        return new ViewHolder(cardView);
    }

    private void updateCardBackgroundColor(ImageCardView view, boolean selected) {
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;
        view.setBackgroundColor(color);
        view.findViewById(R.id.info_field).setBackgroundColor(color);
    }

    @Override
    public void onBindViewHolder(final Presenter.ViewHolder viewHolder, Object item) {
        Video video = (Video) item;

        final ImageCardView cardView = (ImageCardView) viewHolder.view;
        cardView.setTitleText(video.title);
        cardView.setContentText(video.studio);

        int width = cardView.getResources().getDimensionPixelSize(R.dimen.card_width);
        int height = cardView.getResources().getDimensionPixelSize(R.dimen.card_height);
        cardView.setMainImageDimensions(width, height);

        thumbnailToDrawableQueue.AddToQueue(video.videoUrl, new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
            @Override
            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                cardView.setMainImage(youTubeThumbnailView.getDrawable());
                cardView.invalidate();
            }

            @Override
            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
            }
        });
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        ImageCardView cardView = (ImageCardView) viewHolder.view;
        cardView.setBadgeImage(null);
        cardView.setMainImage(null);
    }
}
