package yid.MasterClassLeanback.presenter;

import android.content.Context;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayDeque;

import yid.MasterClassLeanback.ui.VideoDetailsFragment;

public class ThumbnailToDrawableQueue implements YouTubeThumbnailView.OnInitializedListener, YouTubeThumbnailLoader.OnThumbnailLoadedListener {
    private class Item{
        Item(String VideoId, YouTubeThumbnailLoader.OnThumbnailLoadedListener Listener){
            this.VideoId = VideoId;
            this.Listener = Listener;
        }
        final String VideoId;
        final YouTubeThumbnailLoader.OnThumbnailLoadedListener Listener;
    }
    private YouTubeThumbnailView view;
    private ArrayDeque<Item> queue;
    private YouTubeThumbnailLoader loader;
    private YouTubeThumbnailLoader.OnThumbnailLoadedListener curListener;
    private boolean busy = true;
    private boolean initialised = true;
    public ThumbnailToDrawableQueue(Context context){
        queue = new ArrayDeque<>();
        view = new YouTubeThumbnailView(context);
        view.initialize(VideoDetailsFragment.DEVELOPER_KEY,this);
    }

    @Override
    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
        loader = youTubeThumbnailLoader;
        loader.setOnThumbnailLoadedListener(this);
        LoadNext();
    }

    @Override
    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
        busy = true;
        initialised = false;
    }

    public void AddToQueue(String VideoId, YouTubeThumbnailLoader.OnThumbnailLoadedListener Listener){
        queue.addLast(new Item(VideoId, Listener));
        if(!busy){
            LoadNext();
        }
    }
    @Override
    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
        curListener.onThumbnailLoaded(youTubeThumbnailView, s);
        youTubeThumbnailView.setImageDrawable(null);
        LoadNext();
    }

    @Override
    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
        curListener.onThumbnailError(youTubeThumbnailView, errorReason);
        LoadNext();
    }
    private void LoadNext(){
        busy = true;
        Item item = queue.pollFirst();
        if(item == null)
            busy = false;
        else {
            curListener = item.Listener;
            loader.setVideo(item.VideoId);
        }
    }
    public void Release(){
        busy = true;
        queue.clear();
        if(initialised && loader!=null)
            loader.release();
    }
}