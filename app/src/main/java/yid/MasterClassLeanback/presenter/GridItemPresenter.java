package yid.MasterClassLeanback.presenter;

import android.support.v17.leanback.widget.Presenter;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import yid.MasterClassLeanback.R;

public class GridItemPresenter extends Presenter {
    private int mSelectedBackgroundColor = -1;
    private int mDefaultBackgroundColor = -1;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        mDefaultBackgroundColor = parent.getContext().getResources().getColor(R.color.default_background);
        mSelectedBackgroundColor = parent.getContext().getResources().getColor(R.color.video_selected_background);
        TextView view = new TextView(parent.getContext()){
            @Override
            public void setSelected(boolean selected) {
                updateCardBackgroundColor(this, selected);
                super.setSelected(selected);
            }
        };

        int width = parent.getResources().getDimensionPixelSize(R.dimen.card_width);
        int height = parent.getResources().getDimensionPixelSize(R.dimen.card_height);

        view.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setTextColor(parent.getResources().getColor(R.color.right_fragment_background));
        view.setGravity(Gravity.CENTER);
        updateCardBackgroundColor(view, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        ((TextView) viewHolder.view).setText((String) item);
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
    }

    private void updateCardBackgroundColor(View view, boolean selected) {
        int color = selected ? mSelectedBackgroundColor : mDefaultBackgroundColor;
        view.setBackgroundColor(color);
    }
}
