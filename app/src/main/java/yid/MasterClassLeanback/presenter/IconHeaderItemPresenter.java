package yid.MasterClassLeanback.presenter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.RowHeaderPresenter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import yid.MasterClassLeanback.R;

public class IconHeaderItemPresenter extends RowHeaderPresenter {

    private float mUnselectedAlpha;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        mUnselectedAlpha = viewGroup.getResources()
                .getFraction(R.fraction.lb_browse_header_unselect_alpha, 1, 1);
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.icon_header_item, null);
        view.setAlpha(mUnselectedAlpha);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        HeaderItem headerItem = ((ListRow) item).getHeaderItem();
        View rootView = viewHolder.view;
        rootView.setFocusable(true);

        ImageView iconView = (ImageView) rootView.findViewById(R.id.header_icon);
        Drawable icon = null;
        switch (headerItem.getName()){
            case "Избранное": // TODO: 22.10.2016 С одной стороны это hardcode, с другой в switch должны быть константы
                icon = rootView.getResources().getDrawable(R.drawable.preferences);
                break;
            case "Кулинария":
                icon = rootView.getResources().getDrawable(R.drawable.cooking);
                break;
            case "Танцы":
                icon = rootView.getResources().getDrawable(R.drawable.dancing);
                break;
            case "Фитнес":
                icon = rootView.getResources().getDrawable(R.drawable.fitness);
                break;
            case "Программирование":
                icon = rootView.getResources().getDrawable(R.drawable.programming);
                break;
            case "Музицирование":
                icon = rootView.getResources().getDrawable(R.drawable.music);
                break;
            case "Иностранные языки":
                icon = rootView.getResources().getDrawable(R.drawable.foreign_languages);
                break;
            case "Об авторах":
                icon = rootView.getResources().getDrawable(R.drawable.authors);
                break;
        }
        if(icon != null) {
            icon.setColorFilter(rootView.getResources().getColor(R.color.right_fragment_background), PorterDuff.Mode.SRC_IN);
            iconView.setImageDrawable(icon);
        }

        TextView label = (TextView) rootView.findViewById(R.id.header_label);
        label.setText(headerItem.getName());
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        // no op
    }

    @Override
    protected void onSelectLevelChanged(RowHeaderPresenter.ViewHolder holder) {
        holder.view.setAlpha(mUnselectedAlpha + holder.getSelectLevel() * (1.0f - mUnselectedAlpha));
    }
}
