package yid.MasterClassLeanback.presenter;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;

import yid.MasterClassLeanback.model.Video;

public class DetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Video video = (Video) item;

        if (video != null) {
            viewHolder.getTitle().setText(video.title);
            viewHolder.getSubtitle().setText(video.studio);
            viewHolder.getBody().setText(video.description);
        }
    }
}
